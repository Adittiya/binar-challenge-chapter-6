# Binar Academy Full-Stack Web Developer Challange 5

#### Preview

![Project](/public/images/preview.gif)

#### Goals

1. Create a simple monolith dashboard using the view engine.
2. Super User Authentication login dashboard using static data.
3. Create at least three tables for example:
   - User_game table
   - User_game_biodata table
   - User_game_history table
4. The data in the monolith dashboard is connected to the SQL database and has the following methods
   as follows:
   - Create game user data
   - Read user game data
   - Update user game data
   - Delete user game data
5. Design and implement a database schema that links the three tables
   with user_id as the Primary Key (PK).
6. Create a one-endpoint RESTFUL API with CRUD methods (create, read, delete, update).
7. Database attributes are determined by the students themselves.

## Installation

You need to install Node.js, NPM, PostgreSql And Database Client (pgadmin4, dbeaver)

How To Use

- Clone or download Repository
- Open Directory using hyper, ubuntu, git bash, or terminal
- Install package with Run `npm i`
- Run server with `npm start` or `npm run dev`
- Open `http://localhost:3000` and other route in your browser
- login as User static admin or register as new User and will be saved on Database Postgres
- User static email: admin@gmail.com, Password: admin

## Package Used

- Express.js
- body-parser
- fs
- ejs
- express-session
- sequelize
- pg
- bcrypt
