"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const users = require("./public/js/user.js");
const fs = require("fs");
const session = require("express-session");
const { User_game, User_game_biodata, User_game_history } = require("./models");
const bcrypt = require("bcrypt");

const app = express();
const port = 3000;
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(
  session({
    secret: "secret-key",
    resave: false,
    saveUninitialized: false,
  })
);

app.get("/", (req, res) => {
  res.render("home", { user: req.session.user });
});

app.get("/play", (req, res) => {
  if (req.session.loggedIn) {
    res.render("play", { user: req.session.user });
  } else {
    res.redirect("/login");
  }
});

app.get("/register", (req, res) => {
  res.render("register");
});

app.get("/login", (req, res) => {
  res.render("login");
});

app.post("/register", async (req, res) => {
  const { nickname, email, password, fullName, location, phone, dateOfBirth } =
    req.body;

  try {
    // Check if email already exists in the database
    const userExists = await User_game.findOne({ where: { email } });
    if (userExists) {
      return res
        .status(400)
        .send({ status: "error", message: "Email sudah terpakai" });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user
    const newUser = await User_game.create({
      nickname,
      email,
      password: hashedPassword,
    });

    // Create user biodata
    await User_game_biodata.create({
      userId: newUser.id,
      fullName,
      location,
      phone,
      dateOfBirth,
    });

    console.log("Data berhasil disimpan");
    res.status(200).send({
      status: "success",
      message: "Register berhasil, Data disimpan di database",
    });
  } catch (error) {
    console.error(error);
    res.status(500).send({
      status: "error",
      message: "Terjadi kesalahan saat menyimpan data",
    });
  }
});

app.post("/login", async (req, res) => {
  const emailLogin = req.body.email;
  const passwordLogin = req.body.password;

  let user = users.find(
    (user) => user.email === emailLogin && user.password === passwordLogin
  );

  if (!user) {
    user = await User_game.findOne({
      where: {
        email: emailLogin,
      },
      include: { model: User_game_biodata },
    });

    if (!user) {
      return res.send({
        status: "error",
        message: "Username atau password salah!",
      });
    }

    const isMatch = await bcrypt.compare(passwordLogin, user.password);

    if (!isMatch) {
      return res.send({
        status: "error",
        message: "Username atau password salah!",
      });
    }
  }

  req.session.loggedIn = true;
  req.session.user = user;
  console.log("Login Berhasil");
  res.render("home", { user: req.session.user });
});

// route untuk melihat list user
app.get("/users", (req, res) => {
  const usersWithoutPassword = users.map((user) => {
    return {
      nick: user.nick,
      email: user.email,
    };
  });
  res.send({ users: usersWithoutPassword });
});

// route logout
app.get("/logout", (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.send({ status: "error", message: "Logout gagal" });
    } else {
      res.redirect("/");
    }
  });
});

app.get("/userlist", (req, res) => {
  User_game.findAll({ include: User_game_biodata }).then((user_games) => {
    res.render("userlist", {
      user_games: user_games,
    });
  });
});

app.get("/edit/:id", async (req, res) => {
  try {
    const user = await User_game.findByPk(req.params.id, {
      include: User_game_biodata,
    });
    res.render("edit", { user });
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
});

app.post("/edit/:id", async (req, res) => {
  try {
    const userId = req.params.id;
    const { nickname, email, fullName, location, phone, dateOfBirth } =
      req.body;

    // update user fields
    await User_game.update({ nickname, email }, { where: { id: userId } });

    // update user biodata fields
    await User_game_biodata.update(
      { fullName, location, phone, dateOfBirth },
      { where: { userId } }
    );

    res.redirect("/userlist");
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
});

app.get("/delete/:id", async (req, res) => {
  try {
    const userId = req.params.id;
    await User_game.destroy({ where: { id: userId } });
    res.redirect("/userlist");
  } catch (err) {
    console.log(err);
    res.sendStatus(500);
  }
});

app.get("/history/:id", function (req, res) {
  const userId = req.params.id;

  User_game.findOne({ where: { id: userId }, include: User_game_history })
    .then(function (user) {
      res.render("history", { user: user });
    })
    .catch(function (error) {
      console.log(error);
      res.status(500).send("An error occurred");
    });
});

app.get("/addhistory/:id", function (req, res) {
  const userId = req.params.id;

  User_game.findOne({ where: { id: userId } })
    .then(function (user) {
      res.render("addhistory", { user: user });
    })
    .catch(function (error) {
      console.log(error);
      res.status(500).send("An error occurred");
    });
});

app.post("/addhistory/:id", function (req, res) {
  const userId = req.params.id;
  const { gameName, result } = req.body;

  User_game.findOne({ where: { id: userId } })
    .then(function (user) {
      return user.createUser_game_history({ gameName, result });
    })
    .then(function () {
      res.redirect("/history/" + userId);
    })
    .catch(function (error) {
      console.log(error);
      res.status(500).send("An error occurred");
    });
});

app.get("/history/delete/:userId/:gameId", function (req, res) {
  const userId = req.params.userId;
  const gameId = req.params.gameId;

  User_game_history.destroy({ where: { id: gameId } })
    .then(function () {
      res.redirect("/history/" + userId);
    })
    .catch(function (error) {
      console.log(error);
      res.status(500).send("An error occurred");
    });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
